package model;

/**
 * Created by Mala on 3/2/2017.
 */
public class DeveloperProject {
    private int developerId;
    private int projectId;

    public DeveloperProject() {

    }

    public DeveloperProject(int dec_id, int proj_id) {
    }

    public int getDeveloperId() {
        return developerId;
    }

    public void setDeveloperId(int developerId) {
        this.developerId = developerId;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }
}
